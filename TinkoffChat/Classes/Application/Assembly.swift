//
//  Assembly.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

class Assembly {
    
    let loadingImageService: ILoadingImageService
    let imageLoader: IImageLoader
    
    let scalingImageService: IScalingImageService
    let profileService: IProfileService
    let conversationService: IConversationService
    let storage: IStorage
    let communicationLayer: ICommunicationLayer
    let keyboardManager: IQKeyboardManager

    init() {
        let session = URLSession.shared
        let storage = StorageCoreData()
        let communicationLayer = CommunicationLayer()
        let imageLoader = ImageLoader(session: session)
        
        self.profileService = ProfileService(storage: storage)
        self.conversationService = ConversationService(storage: storage, communicationLayer: communicationLayer)
        self.storage = storage
        self.communicationLayer = communicationLayer
        self.keyboardManager = IQKeyboardManager.shared
        self.scalingImageService = ScalingImageService()
        self.imageLoader = imageLoader
        self.loadingImageService = LoadingImageServise(imageLoader: imageLoader)
        
        
    }
}
