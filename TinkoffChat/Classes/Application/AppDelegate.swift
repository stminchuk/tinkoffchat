//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by Станислава on 21.09.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var assembly: Assembly? {
        return ((window?.rootViewController as? UINavigationController)?.topViewController as? ConversationListViewController)?.assembly
    }
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        assembly?.keyboardManager.enable = true
        
        return true
    }
    
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        assembly?.communicationLayer.start()

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        assembly?.conversationService.markAllConversationsOffline()
    }
}
