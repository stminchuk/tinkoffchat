//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Станислава on 21.09.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit
import CoreData

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    // MARK: - UI
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var changeImageButton: UIButton!
    @IBOutlet weak var closeProfileButton: UIBarButtonItem!
    @IBOutlet weak var saveViaOperationsButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var placeholderLabel : UILabel!
    
    // MARK: - Dependencies
    
    var assembly: Assembly!
    lazy var controller: IProfileController = ProfileController(service: assembly.profileService)
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        controller.loadProfile()
        configEditButton()
        configChangeImageButton()
        configPlaceholderLabel()
        configOperationButton()
        configProfileImageView()
        updateData()
    }
    
    
    // MARK: - Taking profile image
    
    private func openPhotoGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        } else {
            showGalleryUnavailableAlert()
        }
    }
    
    private func showGalleryUnavailableAlert() {
        let galleryAlert = UIAlertController(title: "Gallery error", message: "Photo gallery is unavailable", preferredStyle: .alert)
        let galleryOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        galleryAlert.addAction(galleryOk)
        present(galleryAlert, animated: true, completion: nil)
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        } else {
            showNoCameraAlert()
        }
    }
    
    private func showNoCameraAlert() {
        let cameraAlert = UIAlertController(title: "No Camera", message: "This devise has no camera", preferredStyle: .alert)
        let cameraOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        cameraAlert.addAction(cameraOk)
        present(cameraAlert, animated: true, completion: nil)
    }
    
    private func showUnavailableApiAlert() {
        let apiAlert = UIAlertController(title: "No API", message: "API is unavailable", preferredStyle: .alert)
        let apiOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        apiAlert.addAction(apiOk)
        present(apiAlert, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func didTapChangeProfileImageButton(_ sender: Any) {
        print("Please choose an image")
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let chooseFromGalleryButton = UIAlertAction(title: "Choose from gallery", style: .default) { [weak self] _ in
            self?.openPhotoGallery()
        }
        actionSheetController.addAction(chooseFromGalleryButton)
        
        let openCameraButton = UIAlertAction(title: "Take photo", style: .default) { [weak self] _ in
            self?.openCamera()
        }
        actionSheetController.addAction(openCameraButton)
        
        let loadImageButton = UIAlertAction(title: "Load image from internet", style: .default) { [weak self] _ in
            self?.showCollectionView()
        }
        actionSheetController.addAction(loadImageButton)
 
        present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func tapCloseProsileButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapEditButton(_ sender: Any) {
        showEditingState()
    }
    
    @IBAction func tapOperationsButton(_ sender: Any) {
        controller.save(name: nameTextField.text,
                        description: descriptionTextView.text,
                        image: profileImageView.image) { [weak self] message in
                            
                            self?.showSaveAlert(message: message)
                            self?.updateData()
                            self?.showDefaultState()
        }
    }
    
    func showSaveAlert(message: String) {
        let saveAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let saveOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        saveAlert.addAction(saveOk)
        present(saveAlert, animated: true, completion: nil)
    }
    
    func showCollectionView() {
        let vc = CollectionViewController()
        vc.assembly = assembly
        vc.profileViewDelegate = self
        let nc = UINavigationController(rootViewController: vc)
        self.present(nc, animated: true, completion: nil)
    }
    
    // MARK: - Editing
    
    private func changeStatements(statement: Bool) {
        saveViaOperationsButton.isEnabled = statement
        changeImageButton.isEnabled = statement
        if statement {
            activityIndicator.stopAnimating()
        } else {
            activityIndicator.startAnimating()
        }
    }
    
    private func showDefaultState() {
        editButton.isHidden = false
        saveViaOperationsButton.isHidden = true
        changeImageButton.isHidden = true
        nameTextField.isEnabled = false
        descriptionTextView.isEditable = false
    }
    
    private func showEditingState() {
        saveViaOperationsButton.isHidden = false
        editButton.isHidden = true
        changeImageButton.isHidden = false
        nameTextField.isEnabled = true
        nameTextField.becomeFirstResponder()
        descriptionTextView.isEditable = true
        let newPosition = nameTextField.endOfDocument
        nameTextField.selectedTextRange = nameTextField.textRange(from: newPosition, to: newPosition)
    }
    
    // MARK: - Config UI
    
    private func configEditButton() {
        editButton.layer.cornerRadius = 10
        editButton.layer.borderWidth = 1.0
        editButton.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    private func configOperationButton() {
        saveViaOperationsButton.layer.cornerRadius = 10
        saveViaOperationsButton.layer.borderWidth = 1.0
        saveViaOperationsButton.backgroundColor = UIColor(red: 63.0/255.0, green: 120.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        saveViaOperationsButton.setTitleColor(.white, for: .normal)
        saveViaOperationsButton.layer.borderColor = UIColor.white.cgColor
        saveViaOperationsButton.isHidden = true
    }
    
    private func configPlaceholderLabel() {
        placeholderLabel = UILabel()
        placeholderLabel.text = "Description"
        placeholderLabel.font = UIFont.systemFont(ofSize: (descriptionTextView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        descriptionTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (descriptionTextView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
    }
    
    private func configProfileImageView() {
        profileImageView.layer.cornerRadius = 40
    }
    
    private func configChangeImageButton() {
        changeImageButton.layer.cornerRadius = 40
        changeImageButton.backgroundColor = UIColor(red: 63.0/255.0, green: 120.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        changeImageButton.isHidden = true
    }
    
    private func updateData() {
        profileImageView.image = controller.profile.image ?? #imageLiteral(resourceName: "Profile Image")
        nameTextField.text = controller.profile.name
        descriptionTextView.text = controller.profile.description
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
