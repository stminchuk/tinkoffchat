//
//  ProfileController.swift
//  TinkoffChat
//
//  Created by Станислава on 15.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol IProfileController {
    
    /// Profile
    var profile: Profile { get }
    
    /// Loading profile
    func loadProfile()
    
    /// Updating profile
    func save(name: String?, description: String?, image: UIImage?, completion: @escaping (_ message: String) -> Void)
}

class ProfileController: IProfileController {
    
    // Dependencies
    private let service: IProfileService
    
    // Models
    private(set) var profile: Profile
    
    // MARK: - Init
    
    init(service: IProfileService) {
        self.service = service
        self.profile = service.defaultProfile
    }
    
    // MARK: - IProfileController
    
    func loadProfile() {
        if let profile = service.loadProfile() {
            self.profile = profile
        }
    }
    
    func save(name: String?, description: String?, image: UIImage?, completion: @escaping (_ message: String) -> Void) {
        if name == self.profile.name, description == self.profile.description, image == self.profile.image {
            completion("Data didn't change")
            return
        }
        
        let profile = Profile(name: name, description: description, image: image)
        
        service.save(profile: profile) { error in
            DispatchQueue.main.async { [weak self] in
                if let error = error {
                    completion(error.localizedDescription)
                } else {
                    self?.profile = profile
                    completion("Data saved")
                }
            }
        }
    }
}
