//
//  ConversationIncomingMessageCell.swift
//  TinkoffChat
//
//  Created by Станислава on 06.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit

class ConversationIncomingMessageCell: UITableViewCell, IMessageCellConfiguration {
    
    // MARK: - UI
    
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var incomingMessageLabel: UILabel!
    
    var textForMessage: String = "" {
        didSet {
            incomingMessageLabel.text = textForMessage
        }
    }
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let image = UIImage(named: "Incoming Message Image")
        let resultImage = image?.stretchableImage(withLeftCapWidth: 18, topCapHeight: 13)
        bubbleImageView.image = resultImage?.withRenderingMode(.alwaysTemplate)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
