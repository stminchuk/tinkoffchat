//
//  ConversationOutgoingMessageCell.swift
//  TinkoffChat
//
//  Created by Станислава on 06.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit

class ConversationOutgoingMessageCell: UITableViewCell, IMessageCellConfiguration {
    
    // MARK: - UI
    
    @IBOutlet weak var outgoingMessageLabel: UILabel!
    @IBOutlet weak var bubbleImageView: UIImageView!
    
    var textForMessage: String = "" {
        didSet {
            outgoingMessageLabel.text = textForMessage
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let image = UIImage(named: "Outgoing Message Image")
        let resultImage = image?.stretchableImage(withLeftCapWidth: 13, topCapHeight: 13)
        bubbleImageView.image = resultImage
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
