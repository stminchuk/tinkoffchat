//
//  MessageCellConfiguration.swift
//  TinkoffChat
//
//  Created by Станислава on 06.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

protocol IMessageCellConfiguration: class {
    var textForMessage: String { get set }
}
