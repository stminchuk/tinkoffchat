//
//  ConversationController.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import MultipeerConnectivity
import CoreData

protocol IConversationController {
    
    /// Sending message
    func send(_ text: String, to peerName: String)
    
    /// Marking message read
    func markRead(conversation: ConversationEntity)
}

class ConversationController: IConversationController {
    
    // Dependencies
    private let service: IConversationService
    private let storage: IStorage
    
    // MARK: - Init
    
    init(service: IConversationService, storage: IStorage) {
        self.service = service
        self.storage = storage
    }
    

    
    func showMessagesTemplate(by name: String) -> NSFetchRequest<MessageEntity> {
        let sortDescriptor = NSSortDescriptor(key: "messageDate", ascending: false)
        let fetchRequest = NSFetchRequest<MessageEntity>(entityName: String(describing: MessageEntity.self))
        fetchRequest.predicate = NSPredicate(format: "conversation.name == %@", name)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
    func createControllerForMessageEntity (conversation: ConversationEntity?) -> NSFetchedResultsController<MessageEntity> {
        let request = showMessagesTemplate(by: conversation?.name ?? "")
        let controller = NSFetchedResultsController(fetchRequest: request,
                                                    managedObjectContext: storage.container.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        try? controller.performFetch()
        return controller
    }

    // MARK: - IConversationController
    
    func send(_ text: String, to peerName: String) {
        service.send(text, to: peerName)
    }
    
    func markRead(conversation: ConversationEntity) {
        service.markRead(conversation: conversation)
    }
}
