//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by Станислава on 05.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit
import CoreData

class ConversationViewController: UIViewController {
    
    // MARK: - UI
    
    @IBOutlet weak var tableOfMessagesView: UITableView!
    @IBOutlet weak var textMessageField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var attachFileButton: UIButton!
    
    private lazy var titleLabel: UILabel = {
        let titleLabel: UILabel = UILabel()
        titleLabel.backgroundColor = .clear
        titleLabel.text = conversation?.name
        return titleLabel
    }()
    
    private lazy var titleView: UIView = {
        let view = UIView()
        view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        return view
    }()
    
    var assembly: Assembly!
    private let incomingMessageCellIdentifier = String(describing: ConversationIncomingMessageCell.self)
    private let outgoingMessageCellIdentifier = String(describing: ConversationOutgoingMessageCell.self)
    
    private lazy var controller = ConversationController(service: assembly.conversationService, storage: assembly.storage)
    private lazy var fetchResultsController = controller.createControllerForMessageEntity(conversation: conversation)
    private lazy var conversationService: IConversationService = self.assembly.conversationService
    
    var conversation: ConversationEntity?
    
    //var newTitle: UILabel?

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        conversationService.delegate = self
        fetchResultsController.delegate = self
        
        tableOfMessagesView.register(UINib(nibName: outgoingMessageCellIdentifier, bundle: nil), forCellReuseIdentifier: outgoingMessageCellIdentifier)
        tableOfMessagesView.register(UINib(nibName: incomingMessageCellIdentifier, bundle: nil), forCellReuseIdentifier: incomingMessageCellIdentifier)
        
        tableOfMessagesView.dataSource = self
        
        configTableOfMessagesView()
        
        let isOnline = conversation?.isConversationOnline ?? false
        navigationItem.titleView = titleView
        changeTitle(isOnline: isOnline)
        changeSendButton(isEnabled: isOnline)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let conversation = conversation {
            controller.markRead(conversation: conversation)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func sendAction() {
        guard let conversation = conversation, let text = textMessageField.text else { return }
        
        textMessageField.text = ""
        controller.send(text, to: conversation.name ?? "")
        changeSendButton(isEnabled: false)
    }

    @IBAction func textMessageDidBeginEditing(_ sender: UITextField) {
        changeSendButton(isEnabled: true)
    }
    
    @IBAction func textMessageDidEndEditing(_ sender: UITextField) {
        if textMessageField.text == "" {
            changeSendButton(isEnabled: false)
        } else { return }
    }
    

    func changeSendButton(isEnabled: Bool) {
        
        UIView.transition(with: sendMessageButton, duration: 0.5,
                          options: .transitionCrossDissolve,
            animations: {
                self.sendMessageButton.layer.backgroundColor = isEnabled ? UIColor.green.cgColor: UIColor.lightGray.cgColor
        }, completion: { success in
            self.sendMessageButton.isEnabled = isEnabled
        })
        
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.sendMessageButton.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.5) {
                            self.sendMessageButton.transform = CGAffineTransform.identity
                        }
        })
    }
    
    func changeTitle(isOnline: Bool, animated: Bool = false) {
        let duration: TimeInterval = animated ? 0.3 : 0
        UIView.animate(withDuration: duration) {
            if isOnline {
                self.titleLabel.textColor = UIColor(red: 86.0/255, green: 126.0/255, blue: 58.0/255, alpha: 1)
                self.titleLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            } else {
                self.titleLabel.textColor = .black
                self.titleLabel.transform = .identity
            }
        }
     }
    
    // MARK: - Private
    
    private func configTableOfMessagesView() {
        tableOfMessagesView.rowHeight = UITableViewAutomaticDimension
        tableOfMessagesView.estimatedRowHeight = 42
        tableOfMessagesView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
}

// MARK: - UITableViewDataSource

extension ConversationViewController: UITableViewDataSource {
    
    func tableView(_ tableOfMessagesView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableOfMessagesView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = fetchResultsController.object(at: indexPath)
        
        let identifier = message.income ? outgoingMessageCellIdentifier : incomingMessageCellIdentifier
        
        guard let cell = tableOfMessagesView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? (UITableViewCell & IMessageCellConfiguration) else {
            return UITableViewCell()
        }
        
        cell.textForMessage = message.messageText ?? ""
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        return cell
    }
}

// MARK: - IConversationServiceDelegate

extension ConversationViewController: IConversationServiceDelegate {
    func conversationDidUpdate(name: String?, isOnline: Bool) {
        guard name == conversation?.name else { return }
        
        DispatchQueue.main.async {
            self.changeSendButton(isEnabled: isOnline)
            self.changeTitle(isOnline: isOnline, animated: true)
        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension ConversationViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableOfMessagesView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        guard let indexPath = indexPath ?? newIndexPath else { return }
        
        switch type {
        case .update:
            tableOfMessagesView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else { return }
            tableOfMessagesView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            tableOfMessagesView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            tableOfMessagesView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableOfMessagesView.endUpdates()
    }
}
