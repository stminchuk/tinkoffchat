//
//  ConversationListController.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import CoreData
import MultipeerConnectivity

protocol IConversationListController {
    
    /// Marking conversations offline
    func markAllConversationsOffline()
}

class ConversationListController {
    
    // Dependencies
    private let service: IConversationService
    private let storage: IStorage
    
    // MARK: - Init
    
    init(service: IConversationService, storage: IStorage) {
        self.service = service
        self.storage = storage
    }
    
    func createFetchRequest() -> NSFetchRequest<ConversationEntity> {
        let fetchRequest = NSFetchRequest<ConversationEntity>(entityName: String(describing: ConversationEntity.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
    
    func createControllerForConversationEntity () -> NSFetchedResultsController<ConversationEntity> {
        let request = createFetchRequest()
        let controller = NSFetchedResultsController(fetchRequest: request,
                                                    managedObjectContext: storage.container.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        try? controller.performFetch()
        return controller
    }
    
    // MARK: - IConversationListController
    
    func markAllConversationsOffline() {
        service.markAllConversationsOffline()
    }
}
