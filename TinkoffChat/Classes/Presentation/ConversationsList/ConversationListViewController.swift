//
//  ConversationListViewController.swift
//  TinkoffChat
//
//  Created by Станислава on 03.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit
import CoreData

class ConversationListViewController: UIViewController {
    
    // MARK: - UI
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openProfileButton: UIButton!
    

    
    private let identifier = String(describing: ConverationListCell.self)
    
    // MARK: - Dependencies
    
    let assembly: Assembly = Assembly()
    
    private lazy var controller = ConversationListController(service: assembly.conversationService, storage: assembly.storage)
    private lazy var fetchResultsController = controller.createControllerForConversationEntity()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchResultsController.delegate = self
        
        let nibName = String(describing: ConverationListCell.self)
        tableView.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        configTableView()
    }
    
    // MARK: - Actions
    
    @IBAction func tapOpenProfileButton(_ sender: Any) {
        if let nc = UIStoryboard(name: "Profile", bundle: nil).instantiateInitialViewController() as? UINavigationController,
            let vc = nc.topViewController as? ProfileViewController {
            vc.assembly = assembly
            self.present(nc, animated: true, completion: nil)
        }
    }
    
    // MARK: - Private
    
    private func openConversation(_ conversation: ConversationEntity) {
        guard let vc = UIStoryboard(name: "Conversation", bundle: nil).instantiateInitialViewController() as? ConversationViewController
            else { return }
        
        vc.conversation = conversation
        vc.assembly = assembly
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func configTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
    }
}

// MARK: - UITableViewDataSource

extension ConversationListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchResultsController.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ConverationListCell else {
            return UITableViewCell()
        }
        
        let conversation = fetchResultsController.object(at: indexPath)
        cell.name = conversation.name
        cell.online = conversation.isConversationOnline
        cell.hasUnreadMessages = conversation.hasUnreadMessage
        cell.message = conversation.lastMessage
        cell.date = conversation.date
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ConversationListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let conversation = fetchResultsController.object(at: indexPath)
        
        openConversation(conversation)
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension ConversationListViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        guard let indexPath = indexPath ?? newIndexPath else { return }
        
        switch type {
        case .update:
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else { return }
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
