//
//  ConverationListCell.swift
//  TinkoffChat
//
//  Created by Станислава on 03.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit


class ConverationListCell: UITableViewCell, IConversationCellConfiguration {
    
    // MARK: - UI
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var lastMessageTimeDateLabel: UILabel!
    @IBOutlet weak var unreadMessageIndicatorLabel: UILabel!
    
    var name: String? {
        didSet {
            titleLabel.text = name
        }
    }
    
    var message: String? {
        didSet {
            if message != nil {
                lastMessageLabel.text = message
                lastMessageLabel.textColor = UIColor.gray
            } else {
                lastMessageLabel.text = "No messages yet"
                lastMessageLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    var date: Date? {
        didSet {
            guard let date = date else {
                lastMessageTimeDateLabel.text = ""
                return
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = NSCalendar.current.isDateInToday(date)
                ? "HH:mm"
                : "dd MMMM"
            lastMessageTimeDateLabel.text = dateFormatter.string(from: date)
        }
    }
    
   /* var online: Bool = false {
        didSet {
          backgroundColor = online
            ? UIColor(red: 248.0/255.0, green: 250.0/255.0, blue: 224.0/255.0, alpha: 1.0)
            : UIColor.white
        }
    } */
    
    var online: Bool = false {
        didSet {
            if online {
               backgroundColor = UIColor(red: 248.0/255.0, green: 250.0/255.0, blue: 224.0/255.0, alpha: 1.0)
            } else {
                backgroundColor = UIColor.white
            }
        }
    }
    
    var hasUnreadMessages: Bool = false {
        didSet {
            lastMessageLabel.font = hasUnreadMessages
                ? UIFont.boldSystemFont(ofSize: titleLabel.font.pointSize)
                : UIFont.systemFont(ofSize: titleLabel.font.pointSize)
            unreadMessageIndicatorLabel.isHidden = !hasUnreadMessages
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
    }
}
