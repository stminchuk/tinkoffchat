//
//  ConversationCellConfigurationProtocol.swift
//  TinkoffChat
//
//  Created by Станислава on 05.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

protocol IConversationCellConfiguration {
    var name: String? { get set }
    var message: String? { get set }
    var date: Date? { get set }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}
