//
//  ImageModel.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit

struct ImageModel {
    let identifier = String(describing: CollectionViewCell.self)
    var imagesWithUrl: [[String: UIImage]] = []
    var imageUrls: [ImageURL] = []
    var usedIndexes: [Int] = []
    var imageTouched = false
    var cellSize: CGFloat = 0.0
}
