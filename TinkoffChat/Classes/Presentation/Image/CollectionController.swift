//
//  CollectionController.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit


protocol ICollectionController {
    var collectionViewDelegate: CollectionViewController? { get set }
     func loadPreviewImage(index: Int, type: String)
     func loadFullImage(index: Int, type: String, completion: @escaping(() -> Void))
     var imageModel: ImageModel { get set }
}

class CollectionController: ICollectionController {
    
    var imageModel = ImageModel()
    private let service: ILoadingImageService
    
    weak var collectionViewDelegate: CollectionViewController?
    
    init(service: ILoadingImageService) {
        self.service = service
    }
    
    func loadPreviewImage(index: Int, type: String = "preview") {
        if !self.imageModel.imageUrls.isEmpty {
            self.service.loadImage(type: type, imageUrl: self.imageModel.imageUrls[index], completion: {[weak self] (imageDict, error) in
                if error != nil {
                    return
                }
                else if let imageDict = imageDict {
                    guard let fullImageUrl = imageDict.keys.first else {
                        return
                    }
                    guard let imageData = imageDict[fullImageUrl] else {
                        return
                    }
                    guard let image = UIImage(data: imageData) else {
                        return
                    }
                    self?.imageModel.imagesWithUrl.append([fullImageUrl:image])
                    
                    DispatchQueue.main.async {
                        self?.collectionViewDelegate?.collectionView.reloadData()
                    }
                }
            })
        }
    }
    
    func loadFullImage(index: Int, type: String = "largeImage", completion: @escaping(() -> Void)) {
        DispatchQueue.global().async {
            self.service.loadImage(type: type, imageUrl: self.imageModel.imageUrls[index], completion: {[weak self] (imageDict, error) in
                if let error = error {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self?.collectionViewDelegate?.present(alert,animated: true, completion: nil)
                    }
                }
                else if let imageDict = imageDict {
                    guard let fullImageUrl = imageDict.keys.first else {
                        return
                    }
                    guard let imageData = imageDict[fullImageUrl] else {
                        return
                    }
                    guard let image = UIImage(data: imageData) else {
                        return
                    }
                    self?.imageModel.imagesWithUrl.append([fullImageUrl: image])
                    completion()
                }
            })
        }
    }
}
