//
//  ImageViewController.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var assembly: Assembly!
    lazy var controller: ICollectionController = CollectionController(service: assembly.loadingImageService)
    weak var profileViewDelegate: ProfileViewController?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        controller.collectionViewDelegate = self
        let nibName = controller.imageModel.identifier
        collectionView.register(UINib(nibName: nibName, bundle: nil),
                                forCellWithReuseIdentifier: nibName)
        collectionView.delegate = self
        collectionView.dataSource = self
        createBarButton()
        controller.imageModel.cellSize = (UIScreen.main.bounds.width - 20)/3.0
        
        assembly.loadingImageService.loadImageUrl(completion: ({ [weak self] (imageUrls, error) in
            if let error = error {
                DispatchQueue.main.async {
                    let alertLoadingError = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                    alertLoadingError.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self?.present(alertLoadingError, animated: true, completion: nil)
                }
            } else if let imageUrls = imageUrls {
                self?.controller.imageModel.imageUrls = imageUrls
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
            }
        }))
    }
 
    private func createBarButton() {
        let image = UIImage(named: "Stop Icon")?.withRenderingMode(.alwaysOriginal)
        let leftButtonItem = UIBarButtonItem(
            image: image,
            style: .plain,
            target: self,
            action: #selector(closeImageView(sender: ))
        )
        self.navigationItem.leftBarButtonItem = leftButtonItem
    }
    
    @objc func closeImageView(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}



extension CollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          if controller.imageModel.imageTouched == false {
            DispatchQueue.global().async {
                self.controller.loadFullImage(index: indexPath.row, type: "largeImage", completion: ({[weak self] in
                    guard let index = self?.controller.imageModel.imagesWithUrl.count else {
                        return
                    }
                    guard let image = self?.controller.imageModel.imagesWithUrl[index-1].values.first else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.profileViewDelegate?.profileImageView.image = image
                        self?.dismiss(animated: true, completion: nil)
                    }
                    self?.controller.imageModel.imageTouched = false
                }))
            }
            controller.imageModel.imageTouched = true
        }
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.imageModel.imageUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: controller.imageModel.identifier, for: indexPath) as? CollectionViewCell {
            cell.activityIndicator.startAnimating()
            if controller.imageModel.imagesWithUrl.count > indexPath.row {
                guard let fullImageUrl = controller.imageModel.imagesWithUrl[indexPath.row].keys.first else { return CollectionViewCell() }
                guard let image = controller.imageModel.imagesWithUrl[indexPath.row][fullImageUrl] else { return CollectionViewCell() }
                cell.configWithItem(image: assembly.scalingImageService.scaleImage(image: image, size: Int(controller.imageModel.cellSize)))
            } else {
                if !controller.imageModel.usedIndexes.contains(indexPath.row) {
                    controller.loadPreviewImage(index: indexPath.row, type: "preview")
                    controller.imageModel.usedIndexes.append(indexPath.row)
                }
            }
            return cell
        }
        return CollectionViewCell()
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: controller.imageModel.cellSize, height: controller.imageModel.cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

