//
//  IStorageCoreData.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import CoreData

protocol IStorage {
    
    var container: NSPersistentContainer { get set }
    
}
