//
//  Storage.swift
//  TinkoffChat
//
//  Created by Станислава on 01.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class StorageCoreData: IStorage {
        
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: {_, _ in})
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()
    
}
