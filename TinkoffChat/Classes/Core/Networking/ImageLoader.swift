//
//  ImageLoader.swift
//  TinkoffChat
//
//  Created by Станислава on 23.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

private let url = "https://pixabay.com/api/?key=10791113-36fd304fc3412039d528576e0&q=red+flowers&image_type=photo&per_page=200"

class ImageLoader: IImageLoader {
    
    let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    func requestImageUrl(completion: @escaping ([ImageURL]?, Error?) -> Void) {
        guard let url = URL(string: url) else { fatalError() }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(nil, error)
            } else if let data = data {
                let imageResponse = try? JSONDecoder().decode(ImageResponse.self, from: data)
                completion(imageResponse?.hits, nil)
            } else {
                completion(nil, NSError(domain: "", code: 0, userInfo: nil))
            }
        }
        
        dataTask.resume()
    }
    
    func requestImage(type: String, imageUrls: ImageURL, completion: @escaping (([String: Data]?, Error?) -> Void)) {
        var imageStr = ""
        if type == "preview" {
            guard let imageString = imageUrls.previewURL else { return }
            imageStr = imageString
        } else if type == "largeImage" {
            guard let imageString = imageUrls.largeImageURL else { return }
            imageStr = imageString
        }
        guard let url = URL(string: imageStr) else { fatalError() }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(nil, error)
            } else if let imageData = data {
                guard let fullImageUrl = imageUrls.largeImageURL else { return }
                completion([fullImageUrl: imageData], nil)
            }
        }
        dataTask.resume()
    }
}
