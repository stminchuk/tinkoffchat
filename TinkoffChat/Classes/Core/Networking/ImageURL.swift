//
//  File.swift
//  TinkoffChat
//
//  Created by Станислава on 23.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

struct ImageURL: Codable {
    var previewURL: String?
    var largeImageURL: String?
}
