//
//  ImageResponse.swift
//  TinkoffChat
//
//  Created by Станислава on 23.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

struct ImageResponse: Codable {
    var hits: [ImageURL]
}
