//
//  IImageLoader.swift
//  TinkoffChat
//
//  Created by Станислава on 23.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

protocol IImageLoader {
    func requestImageUrl(completion: @escaping ([ImageURL]?, Error?) -> Void)
    func requestImage(type: String, imageUrls: ImageURL, completion: @escaping (([String:Data]?, Error?) -> Void))
}
