//
//  ICommunicationServiceDelegate.swift
//  TinkoffChat
//
//  Created by Станислава on 26.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation


protocol ICommunicationLayerDelegate: class {
    
    /// Browsing
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didFoundPeer peer: Peer)
    
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didLostPeer peer: Peer)
    
    /// Messages
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didReceiveMessage message: Message,
                              from peer: Peer)
}
