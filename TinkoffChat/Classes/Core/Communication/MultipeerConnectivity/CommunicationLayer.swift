//
//  CommunicationService.swift
//  TinkoffChat
//
//  Created by Станислава on 26.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class CommunicationLayer: NSObject {
    
    let session: MCSession
    let peer: MCPeerID
    let browser: MCNearbyServiceBrowser
    let advertiser: MCNearbyServiceAdvertiser
    
    
    var online: Bool = false
    var delegatesHashTable = NSHashTable<AnyObject>.weakObjects()
    var delegates: [ICommunicationLayerDelegate] {
        return (delegatesHashTable.allObjects as? [ICommunicationLayerDelegate]) ?? []
    }
    
    var foundPeers = Set<MCPeerID>()
        
    override init() {
        peer = MCPeerID(displayName: "Стася " + UIDevice.current.name)
        session = MCSession(peer: peer)
        browser = MCNearbyServiceBrowser(peer: peer, serviceType: "tinkoff-chat")
        advertiser = MCNearbyServiceAdvertiser(peer: peer, discoveryInfo: ["userName": "Стася"], serviceType: "tinkoff-chat")

        super.init()
        
        session.delegate = self
        browser.delegate = self
        advertiser.delegate = self
    }
}

extension CommunicationLayer: ICommunicationLayer {
    
    func send(_ message: Message, to peerName: String) {
        guard let data = message.data,
            let peerID = session.connectedPeers.first(where: { $0.displayName == peerName })
            else { return }
        
        do {
            try session.send(data, toPeers: [peerID], with: .reliable)
        } catch {
            print(error)
        }
    }
    
    func add(delegate: ICommunicationLayerDelegate) {
        delegatesHashTable.add(delegate)
    }
    
    func start() {
        advertiser.startAdvertisingPeer()
        browser.startBrowsingForPeers()
        online = true
    }
    
    func stop() {
        advertiser.stopAdvertisingPeer()
        browser.stopBrowsingForPeers()
        online = false
    }
}

extension CommunicationLayer: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print(#function)
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        guard let message = Message(data: data) else { return }
        
        delegates.forEach({ $0.communicationLayer(self, didReceiveMessage: message, from: Peer(peerID)) })
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }
}

extension CommunicationLayer : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        guard !foundPeers.contains(peerID) else { return }
        foundPeers.insert(peerID)
        print(" >>> send from \(peer.displayName)")
        
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 60)
        delegates.forEach({ $0.communicationLayer(self, didFoundPeer: Peer(peerID)) })
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        foundPeers.remove(peerID)
        delegates.forEach({ $0.communicationLayer(self, didLostPeer: Peer(peerID)) })
    }
}

extension CommunicationLayer: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        guard !foundPeers.contains(peerID) else { return }
        foundPeers.insert(peerID)
        
        delegates.forEach({ $0.communicationLayer(self, didFoundPeer: Peer(peerID)) })
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            invitationHandler(true, self.session)
        })
    }
}

