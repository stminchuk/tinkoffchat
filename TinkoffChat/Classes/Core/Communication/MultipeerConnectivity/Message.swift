//
//  Message.swift
//
//  Created by Станислава on 28.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import MultipeerConnectivity

struct Message {
    enum `Type`: String {
        case textMessage = "TextMessage"
    }
    
    let identifier: String
    let text: String
    let timestamp: Date
    let income: Bool
    
    init(text: String) {
        self.identifier = generateMessageId()
        self.text = text
        self.timestamp = Date()
        self.income = false
    }
    
    init?(data: Data) {
        guard let decoded = try? JSONSerialization.jsonObject(with: data, options: []),
            let json = decoded as? [AnyHashable: Any],
            let identifier = json["messageId"] as? String,
            let text = json["text"] as? String
            else { return nil }
        
        self.identifier = identifier
        self.text = text
        self.timestamp = Date()
        self.income = true
    }
    
    var data: Data? {
        let json = [
            "messageId": identifier,
            "text": text,
            "eventType": Type.textMessage.rawValue
        ]
        
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
}

func generateMessageId() -> String {
    let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))"
    let converted = string.data(using: .utf8)?.base64EncodedString()
    
    return converted ?? ""
}
