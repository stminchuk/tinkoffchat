//
//  ICommunicationService.swift
//  TinkoffChat
//
//  Created by Станислава on 26.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol ICommunicationLayer: class {
    
    /// Service delegate
    func add(delegate: ICommunicationLayerDelegate)
    
    /// Online / Offline
    var online: Bool { get set }
    
    func start()
        
    func send(_ message: Message, to peerName: String)
}
