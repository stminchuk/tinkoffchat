//
//  Peer.swift
//
//  Created by Станислава on 28.10.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import MultipeerConnectivity

struct Peer {
    let identifier: String
    let name: String
    
    init(_ peerId: MCPeerID) {
        identifier = String(describing: peerId)
        name = peerId.displayName
    }
}
