//
//  ConversationService.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import MultipeerConnectivity

class ConversationService {
    
    // Dependencies
    weak var delegate: IConversationServiceDelegate?
    let storage: StorageCoreData
    let communicationLayer: ICommunicationLayer
    
    // MARK: - Init
    
    init(storage: StorageCoreData, communicationLayer: ICommunicationLayer) {
        self.storage = storage
        self.communicationLayer = communicationLayer
        self.communicationLayer.add(delegate: self)
    }
    
    // MARK: - Private
    
    private func saveConversation(peer: Peer, isOnline: Bool) {
        storage.container.performBackgroundTask { (backgroundContext) in
            let peerRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ConversationEntity.self))
            peerRequest.predicate = NSPredicate(format: "name == %@", peer.name)
            let result = try? backgroundContext.fetch(peerRequest)
            
            guard let conversation = (result?.first as? ConversationEntity)
                ?? NSEntityDescription.insertNewObject(forEntityName: "ConversationEntity", into: backgroundContext) as? ConversationEntity
                else { return }
            
            conversation.name = peer.name
            conversation.identifier = peer.identifier
            conversation.isConversationOnline = isOnline
            try? backgroundContext.save()
            self.delegate?.conversationDidUpdate(name: conversation.name, isOnline: isOnline)
        }
    }
    
    private func saveMessage(message: Message, to peerName: String) {
        storage.container.performBackgroundTask { (backgroundContext) in
            let peerRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ConversationEntity.self))
            peerRequest.predicate = NSPredicate(format: "name == %@", peerName)
            let result = try? backgroundContext.fetch(peerRequest)
            let conversation = result?.first as? ConversationEntity
            conversation?.hasUnreadMessage = message.income
            conversation?.lastMessage = message.text
            conversation?.date = message.timestamp
            
        let messageEntity = NSEntityDescription.insertNewObject(forEntityName: "MessageEntity",
                                                                    into: backgroundContext) as? MessageEntity
            messageEntity?.identifier = message.identifier
            messageEntity?.income = message.income
            messageEntity?.messageDate = message.timestamp as NSDate
            messageEntity?.messageText = message.text
            messageEntity?.conversation = conversation
            try? backgroundContext.save()
        }
    }
}

// MARK: - IConversationService

extension ConversationService: IConversationService {
    
    func send(_ text: String, to peerName: String) {
        let message = Message(text: text)
        communicationLayer.send(message, to: peerName)
        saveMessage(message: message, to: peerName)
    }
    
    func markAllConversationsOffline() {
        storage.container.performBackgroundTask { (backgroundContext) in
            let peerRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ConversationEntity.self))
            let result = try? backgroundContext.fetch(peerRequest)
            
            let conversations = result?.compactMap({ $0 as? ConversationEntity })
            
            conversations?.forEach({ $0.isConversationOnline = false })
            try? backgroundContext.save()
        }
    }
    
    func markRead(conversation: ConversationEntity) {
        storage.container.performBackgroundTask { (backgroundContext) in
            let peerRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ConversationEntity.self))
            peerRequest.predicate = NSPredicate(format: "name == %@", conversation.name ?? "")
            let result = try? backgroundContext.fetch(peerRequest)
            let conversation = result?.first as? ConversationEntity
            conversation?.hasUnreadMessage = false
            try? backgroundContext.save()
        }
    }
}

// MARK: - ICommunicationLayerDelegate

extension ConversationService: ICommunicationLayerDelegate {
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didFoundPeer peer: Peer) {
        saveConversation(peer: peer, isOnline: true)
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didLostPeer peer: Peer) {
        saveConversation(peer: peer, isOnline: false)
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didReceiveMessage message: Message, from peer: Peer) {
        saveMessage(message: message, to: peer.name)
    }
    
}
