//
//  IConversationService.swift
//  TinkoffChat
//
//  Created by Станислава on 16.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

protocol IConversationServiceDelegate: class {
    func conversationDidUpdate(name: String?, isOnline: Bool)
}

protocol IConversationService {
    
    /// Sending message
    func send(_ text: String, to peerName: String)
    
    /// Marking conversations offline
    func markAllConversationsOffline()
    
    /// Marking conversations read
    func markRead(conversation: ConversationEntity)
    
    /// Delegate
    var delegate: IConversationServiceDelegate? { get set }
}
