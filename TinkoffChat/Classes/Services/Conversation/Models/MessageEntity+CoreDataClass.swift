//
//  MessageEntity+CoreDataClass.swift
//  TinkoffChat
//
//  Created by Станислава on 10.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//
//

import Foundation
import CoreData


public class MessageEntity: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<MessageEntity> {
        return NSFetchRequest<MessageEntity>(entityName: "MessageEntity")
}

    @NSManaged public var messageText: String?
    @NSManaged public var messageDate: NSDate?
    @NSManaged public var income: Bool
    @NSManaged public var identifier: String?
    @NSManaged public var conversation: ConversationEntity?
    
}
