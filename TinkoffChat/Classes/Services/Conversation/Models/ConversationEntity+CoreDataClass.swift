//
//  ConversationEntity+CoreDataClass.swift
//  TinkoffChat
//
//  Created by Станислава on 11.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//
//

import Foundation
import CoreData


public class ConversationEntity: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConversationEntity> {
        return NSFetchRequest<ConversationEntity>(entityName: "ConversationEntity")
}

    @NSManaged public var isConversationOnline: Bool
    @NSManaged public var name: String?
    @NSManaged public var identifier: String?
    @NSManaged public var lastMessage: String?
    @NSManaged public var hasUnreadMessage: Bool
    @NSManaged public var message: NSSet?
    @NSManaged public var date: Date?
    
    
    // MARK: Generated accessors for message
    @objc(addMessageObject:)
    @NSManaged public func addToMessage(_ value: MessageEntity)
    
    @objc(removeMessageObject:)
    @NSManaged public func removeFromMessage(_ value: MessageEntity)
    
    @objc(addMessage:)
    @NSManaged public func addToMessage(_ values: NSSet)
    
    @objc(removeMessage:)
    @NSManaged public func removeFromMessage(_ values: NSSet)

}
