//
//  IScalingImageService.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit

protocol IScalingImageService {
    func scaleImage(image: UIImage, size: Int) -> UIImage
}


