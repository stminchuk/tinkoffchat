//
//  LoadingImageURLService.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

class LoadingImageServise: ILoadingImageService {
    
    private let imageLoader: IImageLoader
    
    init(imageLoader: IImageLoader) {
        self.imageLoader = imageLoader
    }
    
    func loadImageUrl(completion: @escaping (([ImageURL]?, Error?) -> Void)) {
            imageLoader.requestImageUrl(completion: { imageUrl, error in
                if error != nil {
                    completion(nil, error)
                } else if let imageUrl = imageUrl {
                    completion(imageUrl, nil)
                } else {
                    completion(nil, NSError(domain: "", code: 0, userInfo: nil))
                }
            })
    }
    
    func loadImage(type: String, imageUrl: ImageURL, completion: @escaping (([String: Data]?, Error?) -> Void)) {
        self.imageLoader.requestImage(type: type, imageUrls: imageUrl, completion: {(imageDict, error) in
            if error != nil {
                completion(nil, error)
            } else if let imageDict = imageDict {
                completion(imageDict, nil)
            } else {
                completion(nil, NSError(domain: "", code: 0, userInfo: nil))
            }
        })
    }
    
}
