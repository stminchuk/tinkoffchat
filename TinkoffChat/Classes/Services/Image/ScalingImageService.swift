//
//  ResizingImageService.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit

class ScalingImageService: IScalingImageService {
    func scaleImage(image: UIImage, size: Int) -> UIImage {
        let scale: CGFloat = 0.0
        UIGraphicsBeginImageContextWithOptions(CGSize(width: size, height: size), true, scale)
        image.draw(in: CGRect(origin: .zero, size: CGSize(width: size, height: size)))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = scaledImage else {
            return UIImage()
        }
        return image
    }
}
