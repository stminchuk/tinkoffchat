//
//  ILoadingImageURLService.swift
//  TinkoffChat
//
//  Created by Станислава on 24.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation


protocol ILoadingImageService {
    func loadImageUrl(completion: @escaping (([ImageURL]?, Error?) -> Void))
    func loadImage(type: String, imageUrl: ImageURL, completion: @escaping (([String: Data]?, Error?) -> Void))
}
