//
//  Profile+CoreDataClass.swift
//  TinkoffChat
//
//  Created by Станислава on 01.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//
//

import Foundation
import CoreData

@objc(ProfileEntity)
public class ProfileEntity: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProfileEntity> {
        return NSFetchRequest<ProfileEntity>(entityName: "ProfileEntity")
    }
    
    @NSManaged public var name: String?
    @NSManaged public var descript: String?
    @NSManaged public var profileimage: NSData?
}
