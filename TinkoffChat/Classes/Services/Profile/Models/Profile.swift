//
//  Profile.swift
//  TinkoffChat
//
//  Created by Станислава on 02.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import MultipeerConnectivity
import CoreData

struct Profile {
    let name: String?
    let description: String?
    let image: UIImage?
}
