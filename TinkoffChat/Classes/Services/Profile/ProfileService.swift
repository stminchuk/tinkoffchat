//
//  ProfileService.swift
//  TinkoffChat
//
//  Created by Станислава on 15.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ProfileService: IProfileService {
    
    // Dependencies
    let storage: StorageCoreData
    
    init(storage: StorageCoreData) {
        self.storage = storage
    }
    
    // MARK: - IProfileService
    
    var defaultProfile: Profile {
        return Profile(name: "Стася",
                       description: nil,
                       image: nil)
    }
    
    /// Loading profile
    func loadProfile() -> Profile? {
        let request = NSFetchRequest<ProfileEntity>(entityName: String(describing: ProfileEntity.self))
        
        guard let result = try? storage.container.viewContext.fetch(request),
            let loadedProfile = result.first else {
            return nil
        }
        
        var image: UIImage? = nil
        if let data = loadedProfile.profileimage {
            image = UIImage(data: data as Data)
        }
        
        return Profile(name: loadedProfile.name,
                       description: loadedProfile.descript,
                       image: image)
    }
    
    /// Updating profile
    func save(profile: Profile, completion: @escaping (Error?) -> Void) {
        storage.container.performBackgroundTask { (backgroundContext) in
            do {
                // cleanup
                let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ProfileEntity.self))
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                try backgroundContext.execute(deleteRequest)
                
                // adding
                let dbProfile = NSEntityDescription.insertNewObject(forEntityName: "ProfileEntity", into: backgroundContext) as? ProfileEntity
                dbProfile?.name = profile.name
                dbProfile?.descript = profile.description
                if let image = profile.image {
                    dbProfile?.profileimage = UIImageJPEGRepresentation(image, 1) as NSData?
                }
        
                // saving context
                try backgroundContext.save()
                completion(nil)
            }
            catch {
                completion(error)
            }
        }
    }
}
