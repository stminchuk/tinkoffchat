//
//  IProfileService.swift
//  TinkoffChat
//
//  Created by Станислава on 15.11.2018.
//  Copyright © 2018 Stminchuk. All rights reserved.
//

import Foundation

/// Сервис для работы с профилем
protocol IProfileService {
    
    /// Profile by default
    var defaultProfile: Profile { get }
    
    /// Loading profile
    func loadProfile() -> Profile?
    
    /// Updating profile
    func save(profile: Profile, completion: @escaping (Error?) -> Void)
}
